import React from 'react'
import axios from 'axios'
import { useEffect,useState } from 'react'

const fetchData = [
    {
        id:0,
        sTitle:"PL 2021: Top 5 batsmen with highest individual scores in IPL history",
        sDescription:"In the T20 format, if a player scores a century for the team, then most of the time it becomes quite easy to win the match as well. The same is applicable in global T20 tournaments like  IPL  as well. Though it is not so common to see a player hitting a ton in the 20-over format, some cricketers have enjoyed big innings when it&#8217;s their day. In this article, we are going to list out the 5 ",
        sImage:"https://image.crictracker.com/wp-content/uploads/2020/09/AB-de-Villiers-1.jpg",
        nViewCounts:1530,
    }
]

const tableColumn = [
	{
		Header: 'Image',
		accessor:'sImage',
		Cell: ({row}) => 
			<img
				src={row.original.sImage}
				width={60}
				alt={'sport Image'}
			/>
		
	},
	{
		Header: 'Title',
		accessor: 'sTitle',
	},
	{
		Header: 'Description',
		accessor: 'sDescription',
	},
		
	{
		Header: 'View Counts',
		accessor: 'nViewCounts',
	},
	
]
// {}
//  []
const Post=()=> {
    const  [posts,setPosts] = useState([]);
    
    useEffect(()=>{
        axios.get("https://backend.sports.info/api/v1/posts/recent")
        .then(res=>{
            console.log(res.data.data);  
            // setPosts((prev)=> [...prev,res.data.data]);
            setPosts(res.data.data)
        })
        
    }, [])

    

    console.log(posts)
  return (
    <div>
        {
         posts.length >0 &&(
             
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Description</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>View Counts</th>
                        
                    </tr>
                </thead>
                <tbody>
                    {
                        posts.map((post) =>(
                            <tr key={post?._id}>
                                 {console.log('post', post)}
                                <td>{post?._id}</td>
                                <td>{post?.sDescription}</td>
                                <td>{post?.sTitle}</td>
                                <td>{post?.sImage}</td>                                
                                <td>{post?.nViewCounts}</td>                                
                            </tr>
                        ))
                    }
                </tbody>
            </table>
         )       
        }
      
    </div>
  )
}

export default Post
